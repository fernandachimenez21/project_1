-- ---------------------------------------------------------
-- CRIANDO E USANDO O DATABASE TERAPIA ASSISTIDA POR ANIMAIS
-- ---------------------------------------------------------
create database Controle_IAA;
use Controle_IAA;

drop database Controle_IAA;

-- ---------------------------------------------------------
-- CRIANDO TABELA VOLUNTÁRIO
-- ---------------------------------------------------------
create table Voluntario (
idvoluntario int primary key auto_increment,
nomevoluntario varchar (50),
cpfvoluntario varchar (11),
idadevoluntario date,
enderecovoluntario varchar (150),
email varchar (50),
senha varchar (100)
);
	
select * from Voluntario;

-- ANOS DISTINTOS
select DISTINCT (idadevoluntario) from Voluntario;

-- ORDENANDO POR REGISTROS
select nomevoluntario,cpfvoluntario from `Voluntario` order by nomevoluntario;

-- FORMULA PARA CÁLCULO DA IDADE / ARREDONDA
select round(DATEDIFF(date_format(now(), '%Y-%m-%d'), idadevoluntario )/365) as idade FROM Voluntario;

-- FORMULA PARA CÁLCULO DA IDADE / MAXIMO
select max(round(DATEDIFF(date_format(now(), '%Y-%m-%d'), idadevoluntario )/365)) as idade FROM Voluntario;
