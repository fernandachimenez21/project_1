-- Criando o database e usando TerapiaAssistida
-- --------------------------------------------
create database TerapiaAssistida;
use TerapiaAssistida;
drop database TerapiaAssistida;
-- --------------------------------------------
-- Criando a Tabela Assistidos
-- --------------------------------------------
create table Assistidos (
idAssistido int primary key not null auto_increment,
NomeAssistido varchar (35),
TelefoneAssistido varchar (40),
CPFAssistido varchar (11)
) auto_increment = 10;
-- ---------------------------------------------
-- Criando a tabela Instituição
-- ---------------------------------------------
create table Instituição (
idInstituição int primary key auto_increment,
NomeInstituição varchar (40),
CNPJ varchar (20),
EndereçoInstituição varchar (100),
ValorPagoPorVisita double
) auto_increment = 100;
-- ----------------------------------------------
-- Criando a tabela Cães
-- ----------------------------------------------
create table Cães (
idCão int primary key auto_increment,
NomeCão varchar (30),
IdadeCão varchar (40),
TutorDoCão varchar (50),
fk_Assistido int,
foreign key (fk_Assistido) references Assistidos (idAssistido)
)auto_increment = 200;
-- ----------------------------------------------
-- Criando a tabela associativa AGENDAMENTO
-- ----------------------------------------------
create table Agendamento (
fk_Cão int,
foreign key (fk_Cão) references Cães (idCão),
fk_Instituição int,
foreign key (fk_Instituição) references Instituição (idInstituição),
DataDaVisita date
);
-- --------------------------------------------
-- Selects Individuais
-- -------------------------------------------
select * from Assistidos;
select * from Instituição;
select * from Cães;
select * from Agendamento;
-- --------------------------------------------
-- Descrevendo os tipos das tabelas
-- --------------------------------------------
desc Assistidos;
desc Instituição;
desc Cães;
desc Agendamento;
-- ---------------------------------------------
-- Inserts das tabelas
-- ---------------------------------------------
insert into Assistidos values
(10, 'Lucas Oliveira', '11 5453-4545', '32145687633'), (11, 'Barbara Schimidt', '11 5634-6574',
'98745808134'),
(12, 'Astolfo Cruz', '11 5672-1111', '65734567523'), (13, 'Bruna Gomes', '11 2222-3333',
'2134872340'),
(14, 'Lucio Mauro', '11 4444-5555', '98778901233'), (15, 'Mauro Souza', '11 6666-6666',
'44433322222'),
(16, 'Augusto Santos', '11 7654-1234', '54367895434'), (17, 'Gumercinda Barbosa', '11 2345-
5554', '56434589033'),
(18, 'Maria Silveira', '11 2314-7654', '12312312344'), (19, 'Deolinda Menezes', '11 7890-6543',
'32145687633'),
(20, 'Pedro Gonçalves', '11 2345-9876', '66677787600'), (21, 'Teotônio Luzinalvo', '11 6753-
8765', '99978655544'),
(22, 'Juliette Paes', '11 5677-4532', '76533267022'), (23, 'Paloma Fagundes', '11 3322-6432',
'00075215477');
insert into Instituição values
(null, 'FIC', '0439816898', 'Rua Pedro Alves, 39 - SP', '400.00'), (null, 'Hospital da Clinicas',
'0439816893', 'Rua Lucianda, 43 - SP', '250.00'),
(null, 'Beneficiencia Portuguesa', '0439816892', 'Avenida Paulista, 4532 - SP', '100.90'), (null,
'Bruna Gomes', '3333816822', 'Travessa Luz Verde, 44 - SP', '80.28'),
(null, 'Lar Santo Alberto', '0439816122', 'Travessa Eduardo Serverino, 765 - SP', '45.50'), (null,
'ASA', '6666816898', 'Rua Lopes, 32 - SP', '78.90'),
(null, 'ITACI', '2234512333', 'Rua Lorraine Pamonha, 87 - SP', '34.65'), (null, 'INATAA',
'0439816898', 'Avenida Brigadeiro, 6789 - SP', '100.67'),
(null, 'ILPI para Idosos', '0439815678', 'Travessa Estrela. 7654 - SP', '45.34'), (null, 'GRAAC',
'0439816898', 'Avenida Barreto, 4322 - SP', '765.87'),
(null, 'Hospital de Guarulhos', '0739817555', 'Avenida Itaberaba, 7687 - SP', '54.22'), (null,
'Clinica Feliz', '6639816898', 'Rua Bruneta, 76 - SP', '32.12'),
(null, 'Lar Vida Feliz', '0639816898', 'Alameda Cruzeiro, 76552 - SP', '87.22'), (null, 'Hospital
Bem-Vindo', '0239816898', 'Alameda Oceano, 65 - SP', '55.00');
insert into Cães values
(null, 'Angel', '2 anos', 'Patricia', '10'), (null, 'Koda', '3 anos', 'Beatriz', '10'),
(null, 'Fuinha', '3 anos', 'Andressa', '10'), (null, 'Bia', '1 ano', 'Karolline', '11'),
(null, 'Sultão', '11 meses', 'Fernanda', '12'), (null, '2 anos', '6666816898', 'Luiz Felipe', '13'),
(null, 'Fumaça', '09 meses', 'Matheus', '14'), (null, 'Frajola', '4 anos', 'Stefanie', '15'),
(null, 'Paçoca', '4 anos', 'Kaio', '16'), (null, 'Morgy', '6 anos', 'Bruna', '17'),
(null, 'Lady', '5 anos', 'Amanda', '18'), (null, 'Rex', '7 anos', 'Felipe', '19'),
(null, 'Estrela', '5 anos', 'Manoela', '23'), (null, 'Rex', '2 anos', 'Gustavo', '22'),
(null, 'Nutella', '6 anos', 'Jonas', '20'), (null, 'Oreo', '8 anos', 'Gabriel', '21');
insert into Agendamento values
(200, 100, '2021-11-28'), (200, '101', '2021-05-01'),
(201, 102, '2021-04-15'), (201, '103', '2021-12-17'),
(202, 100, '2021-05-14'), (208, '113', '2021-11-26'),
(209, 112, '2021-06-13'), (210, '112', '2021-10-22'),
(200, 100, '2021-07-12'), (212, '108', '2021-08-24'),
(200, 105, '2021-08-11'), (200, '107', '2021-08-10'),
(200, 101, '2021-09-10'), (200, '104', '2021-06-12');
-- ---------------------------------------------------
-- SUM, MAX, AVG, ORDER BY, MIN e SUBQUERY
-- ---------------------------------------------------
select max(ValorPagoPorVisita) as 'Valores Maiores' from Instituição;
select min(ValorPagoPorVisita) as 'Valores Menores' from Instituição;
select avg(ValorPagoPorVisita) as 'Average dos Valores' from Instituição;
select sum(ValorPagoPorVisita) as 'Soma dos Valores' from Instituição;
select distinct (ValorPagoPorVisita) as 'Valores Distintos/Diferentes' from Instituição order by
ValorPagoPorVisita;
select Assistidos.* from Assistidos order by idAssistido desc;
select Assistidos.* from Assistidos order by idAssistido desc;
select * from Cães inner join Agendamento on fk_Cão = idCão where NomeCão = 'Morgy';
select * from Cães inner join Assistidos on fk_Assistido = idAssistido;