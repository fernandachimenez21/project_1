	'use strict';

	/* 
	lista e explicação dos Datatypes:
	https://codewithhugo.com/sequelize-data-types-a-practical-guide/
	*/

	module.exports = (sequelize, DataTypes) => {
		let Usuario = sequelize.define('Usuario', {
			idvoluntario: {
				field: 'idvoluntario',
				type: DataTypes.INTEGER,
				primaryKey: true,
				autoIncrement: true
			},
			nomevoluntario: {
				field: 'nomevoluntario',
				type: DataTypes.STRING,
				allowNull: false
			},
			cpfvoluntario: {
				field: 'cpfvoluntario',
				type: DataTypes.STRING,
				allowNull: false
			},
			idadevoluntario: {
				field: 'idadevoluntario',
				type: DataTypes.DATEONLY,
				allowNull: false
			},
			enderecovoluntario: {
				field: 'enderecovoluntario',
				type: DataTypes.STRING,
				allowNull: false
			},
			email: {
				field: 'email',
				type: DataTypes.STRING,
				allowNull: false
			},
			senha: {
				field: 'senha',
				type: DataTypes.STRING,
				allowNull: false
			}

		}, {
			tableName: 'Voluntario',
			freezeTableName: true,
			underscored: true,
			timestamps: false,
		});

		return Usuario;
	};